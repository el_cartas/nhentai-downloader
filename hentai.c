#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pcre.h>
#include <curl/curl.h>

#include <sys/stat.h>
#include <getopt.h>

#define PAGINA  "https://nhentai.net/g/"
#define IMAGENES        "https://i.nhentai.net/"

#define OVECCOUNT       30

const char* sacar_paginas = "num-pages\">([0-9]*)</span>";
const char* imagenes = "galleries/([0-9]*)/([0-9]*)[.]([a-z]*)";
const char* titulos = "title\" content=\"(.*)\" /><meta property";
const char* tags = "description\" content=\"(.*)\" /><meta name=\"viewport";

const char* extension_regex = "jpg|jpeg|png|gif";

struct MemoryStruct
{
	char* memory;
	size_t size;
};

struct option long_option[]=
{
	{"id",	required_argument,	0,	'i'},
	{"output",	required_argument,	0,	'o'},
	{"help",	no_argument,	0,	'h'},
	{0,0,0,0}
};

char* link (char* numeros, int pag);
char* base (char* in, CURL* code);
char* reg_comp_codes (char* data, const char* regex ,int num);
void curleada(CURL* in, char* link_1, char* link_2);
int help(char* program_name);
size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream);
char* file_extension(char*);

static size_t Callback(void* contents, size_t size, size_t nmeb, void* userp)
{
	size_t realsize = size * nmeb;
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;

	char* ptr = realloc(mem->memory, mem->size + realsize + 1);

	if(ptr == NULL) {printf("Sin memorya suficiente\n"); return 0;}

	mem->memory = ptr;
	memcpy (&(mem->memory[mem->size]), contents, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;
}

int main(int argc, char* argv[])
{
	if (argc < 2) { help(argv[0]); return 0;}

	int opt, opt_index = 0;
	char h_codes[8];

	bool cod = false;
	bool ot = false;

	char* directorio;

	while((opt = getopt_long (argc,argv,":i:o:h", long_option, &opt_index)) != -1) {

	switch(opt){

		case 'i':{
			cod = true;
			snprintf(h_codes, sizeof(h_codes), "%s", optarg);
		}break;

		case 'o':{
			ot = true;
			size_t directorio_size = strlen(optarg) +1;

			if(optarg[strlen(optarg)-1] != '/'){
				directorio = (char*) malloc(directorio_size) +1;
				snprintf(directorio, directorio_size +1, "%s/", optarg);
				}else{
				directorio = (char*) malloc(directorio_size);
				snprintf(directorio, directorio_size, "%s", optarg);}
		 }break;

		 case 'h':{ help(argv[0]); }break;

		 }
	}

	if (cod != true) {snprintf(h_codes, sizeof(h_codes), "%s", argv[1]);}

	CURL * curl_handle;
	curl_handle = curl_easy_init();

	char* hentai_1 = link(h_codes, 1);
	char* hen_titulos = link(h_codes, 0);

	//printf("%s\n%s\n", hentai_1, hen_titulos);

	int p = atoi(reg_comp_codes(base(hentai_1,curl_handle),sacar_paginas,1));
	char* nombres = reg_comp_codes(base(hen_titulos,curl_handle),titulos, 1);	
	char* tgs = reg_comp_codes(base(hen_titulos,curl_handle), tags, 1);
	
	int l;

	printf("Estas descargando: %s\n", nombres);
	printf("Tags: %s\n\n", tgs);

	if(ot != true){ 
	char* di = "./";
	directorio = (char*) malloc(strlen(di)+1);
	memcpy(directorio, di ,strlen(di) +1 );}

	size_t directorio_buf = strlen(directorio) + strlen(nombres) + 1 ;
	char* output_dir = (char*) malloc(directorio_buf);
	snprintf(output_dir, directorio_buf, "%s%s", directorio, nombres);

	free(tgs);
	free(nombres);

	mkdir(output_dir,S_IRWXU);
	
	//una aproximación, para no tener que hacer malloc todo el rato
	char nom[directorio_buf + 8];
	char* test;
	char* hentai_2;
	size_t test_buf;
	char* new_codes;
	char* extension;

	for (l =1; l < p+1; l++)
	{
		printf("\r%d/%d\t", l, p);
		hentai_2 = link(h_codes,l);

		new_codes = reg_comp_codes(base(hentai_2, curl_handle), imagenes, 0);
		test_buf = strlen(new_codes) + strlen(IMAGENES) + 1;
		test = (char*) malloc(test_buf);

		snprintf(test, test_buf, "%s%s", IMAGENES, new_codes);
		free(new_codes);
		//"loading screen" o como le digan re feo, pero algo es algo
		printf("\r%d/%d\t%s", l, p, test);
		extension = reg_comp_codes(test, extension_regex, 0);
		fflush(stdout);
		snprintf(nom, sizeof(nom), "%s/%d.%s",output_dir, l, extension);
		curleada(curl_handle, test, nom);
		free(test);
		free(hentai_2);
		free(extension);
		curl_easy_setopt(curl_handle, CURLOPT_TCP_KEEPALIVE, 1L);
	}
	printf("\n");
	
	free(hentai_1);
	free(hen_titulos);
	//free(directorio);
	free(output_dir);
	curl_global_cleanup();
	return 0;
				
}

char* link (char* numeros, int pag)
{
	char* codigo;
	if(pag > 9999){printf("Que onda, esto debe ser un bug lol\n");exit(EXIT_FAILURE);}

	if (pag == 0){

	size_t codigo_buf = strlen(PAGINA) + strlen(numeros) + 2;
	codigo = (char*) malloc(codigo_buf);
	snprintf(codigo, codigo_buf,"%s%s/", PAGINA, numeros);
	
	}else{

	size_t codigo_buf = strlen(PAGINA) + strlen(numeros) + sizeof(pag) + 3;
	codigo = (char*) malloc(codigo_buf);
        snprintf(codigo, codigo_buf, "%s%s/%d/", PAGINA, numeros, pag);}
	
	return codigo;
}

char* base (char* in, CURL* code)
{
	struct MemoryStruct chunk;
	chunk.memory = malloc(1);
	chunk.size = 0;

	curl_easy_setopt(code, CURLOPT_URL, in);
	curl_easy_setopt(code, CURLOPT_WRITEFUNCTION, Callback);
	curl_easy_setopt(code, CURLOPT_WRITEDATA, (void *)&chunk);
	curl_easy_setopt(code, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/81.0");

	curl_easy_perform(code);

	if (chunk.size < 4000){ printf("Codigo invalido\n"); exit(EXIT_FAILURE);}

	return chunk.memory;
}

char* reg_comp_codes (char* data, const char* regex ,int num)
{
	pcre* re;
	const char* error;
	int erroffset;
	int ovector[OVECCOUNT];
	int rc = 0;

	re = pcre_compile(regex, 0, &error, &erroffset, NULL);

	if (re == NULL){ printf("Hay un error con el regex\n"); exit(EXIT_FAILURE);}

	rc = pcre_exec(re, NULL, data, (int)strlen(data), 0, 0, ovector, OVECCOUNT);

	if (rc < 0) {
		switch(rc){
			case PCRE_ERROR_NOMATCH: printf("No match\n"); break;
			default: printf("Matching error %d\n", rc); break;
			}
		pcre_free(re);
		exit(EXIT_FAILURE);
		}

	char* substring_start = data + ovector[2*num];
	int substring_length = ovector[2*num+1] - ovector[2*num];

	char* output_data = (char*) malloc(substring_length+1);

	strncpy (output_data, substring_start, substring_length);
	output_data[substring_length] = '\0';
	
	return output_data;
}

void curleada(CURL* in, char* link_1, char* link_2)
{
	curl_easy_setopt(in, CURLOPT_URL, link_1);
	curl_easy_setopt(in, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/81.0");
	curl_easy_setopt(in, CURLOPT_WRITEFUNCTION, write_data);

	 FILE* pagefile = fopen(link_2, "wb");
	 if (pagefile){

	 	curl_easy_setopt(in, CURLOPT_ACCEPT_ENCODING, "gzip");
		curl_easy_setopt(in, CURLOPT_WRITEDATA, pagefile);

		curl_easy_perform(in);

		fclose(pagefile);
		}else{
		printf("Ha ocurrido un error con la descarga\n");
		exit(EXIT_FAILURE);}

}

int help(char* program_name)
{
	 printf("\n Opciones disponibles: \n[-h|--help] Muestra este mensaje de \"ayuda\" \n[-o|--output] Para elegir el direcctorio\
	 de salida \n[-i|--id] Para elegir el codigo de el hentai que se quiere descargar \n\nEjemplo:  %s --id [codigo] -- output\
	 <directorio> \n%s [codigo]\n\nGracias especiales para Renich, Ashandme, Kmosq, Negrosido, Sprite y Pablon por haber ayudado\
	 de una u otra forma a el desarrollo de estre programa\n\n", program_name, program_name);
	 exit(0);
}

size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
{
size_t written = fwrite(ptr, size, nmemb, (FILE *)stream);
return written;
}
